package com.mallplus.common.entity.fenxiao.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mallplus.common.entity.fenxiao.entity.FenxiaoRecords;


/**
 * @author mallplus
 * @date 2019-12-17
 */
public interface FenxiaoRecordsMapper extends BaseMapper<FenxiaoRecords> {
}
