package com.mallplus.common.entity.fenxiao.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mallplus.common.entity.fenxiao.entity.FenxiaoChecks;


/**
 * @author mallplus
 * @date 2019-12-17
 */
public interface FenxiaoChecksMapper extends BaseMapper<FenxiaoChecks> {
}
